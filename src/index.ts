import "module-alias/register";
import { Routes } from "@interfaces/http/Router";
import * as bodyParser from "body-parser";
import express from "express";

class App {

    public app: express.Application = express();
    public router: Routes = new Routes();

    constructor() {
        this.config();
        this.router.routes(this.app);
    }

    private config(): void {
        this.app.use(bodyParser.json({limit: "50mb"}));
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

}

export default new App().app;

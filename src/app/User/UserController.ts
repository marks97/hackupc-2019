import { User } from "@models/User";

export default class UserController {

    public async getUser(data) {
        const user = await User.findOne({
            where: {
                personId: data,
            },
        });
        console.log(user.name);
        return user.name;
    }
}

export default class Application {

    private server;

    constructor({ server }) {
        this.server = server;
    }

    public async start() {
        await this.server.start();
    }
}

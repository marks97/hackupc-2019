import { database } from "@infra/config/database";
import { BuildOptions, DataTypes, Model, Sequelize } from "sequelize";

export class User extends Model {
    public tokenID: number;
    public faceId: number;
    public name: string;
}

User.init(
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: new DataTypes.STRING(128),
            allowNull: false,
        },
    },
    {
        tableName: "user",
        sequelize: database,
    },
);

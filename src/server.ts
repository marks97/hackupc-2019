import * as http from "http";
import app from "./index";
const PORT = 3000;

http.createServer(app).listen(PORT, () => {
    console.log("\nExpress server listening on port " + PORT + "\n");
});

import UserController from "@app/User/UserController";
import { base64ToFile, uploadImageToAWS } from "@infra/ImageUtils";
import { getFaceId } from "@infra/RecognitionUtils/FaceDetection";
import {getMatchFace} from "@infra/RecognitionUtils/FaceVerify";
import {Request, Response} from "express";

export class Routes {
    public routes(app): void {
        app.route("/").post(async (req: Request, res: Response) => {
            const userController = new UserController();
            const imagePath = await base64ToFile(req.body.image);
            const imageUrl = await uploadImageToAWS(imagePath);
            const faceId = await getFaceId(imageUrl);
            const personId = await getMatchFace(faceId);
            const userData = await userController.getUser(personId);
            res.status(200).send({
                name: userData,
            });
        });
        app.route("/").get(async (req: Request, res: Response) => {
            res.status(200).send({
                message: "Working",
            });
        });
    }
}

import AWS from "aws-sdk";
import fs from "fs";
import randtoken from "rand-token";
// @ts-ignore
import Promise from "ts-promise";

export function base64ToFile(data: string): Promise<string> {
    return new Promise(async (resolve, reject) => {

        try {
            const base64Data = data.replace(/^data:image\/png;base64,/, "");
            const imagePath = "/home/node/uploads/" + randtoken.generate(16) + ".png";
            await fs.writeFileSync(imagePath, base64Data, "base64");
            resolve(imagePath);
        } catch (e) {
            reject(e);
        }
    });
}

export function uploadImageToAWS(path: string): Promise<string> {

    return new Promise(async (resolve, reject) => {

        const BUCKET = "copenhacks19-repo/HackUpc2019";
        const REGION = "eu-west-1";
        const ACCESS_KEY = "AKIAJQL6N3JFZGER62IQ";
        const SECRET_KEY = "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";

        AWS.config.update({
            accessKeyId: ACCESS_KEY,
            secretAccessKey: SECRET_KEY,
            region: REGION,
        });

        const s3 = new AWS.S3();
        const imageRemoteName = randtoken.generate(16) + ".png";

        s3.putObject({
            Bucket: BUCKET,
            Body: fs.readFileSync(path),
            Key: imageRemoteName,
        })
            .promise()
            .then((response) => {
                resolve("https://copenhacks19-repo.s3.eu-central-1.amazonaws.com/HackUpc2019/" + imageRemoteName);
            })
            .catch((err) => {
                reject(err);
            });
    });
}

import request from "request";
// @ts-ignore
import Promise from "ts-promise";

export function getFaceId(imageUrl: string): Promise<string> {
    return new Promise(async (resolve, reject) => {

        const subscriptionKey = "64af1d5595a6400e907d84a461a3eede";
        const uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0";

        const options = {
            uri: uriBase + "/detect",
            qs: {
                returnFaceId: true,
            },
            body: '{"url": ' + '"' + imageUrl + '"}',
            headers: {
                "Content-Type": "application/json",
                "Ocp-Apim-Subscription-Key" : subscriptionKey,
            },
        };

        request.post(options, (error, response, body) => {
            if (error) {
                console.log("Error: ", error);
                reject(error);
            }

            const jsonResponse = JSON.parse(body);
            resolve(jsonResponse[0].faceId);

        });
    });
}

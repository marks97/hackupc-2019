import request from "request";
// @ts-ignore
import Promise from "ts-promise";

export function getMatchFace(faceId: string): Promise<string> {

    return new Promise(async (resolve, reject) => {

        const subscriptionKey = "64af1d5595a6400e907d84a461a3eede";
        const uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0";

        const params_identify = {
            "largePersonGroupId": "bd-faces",
            "faceIds": [
                faceId,
            ],
            "maxNumOfCandidatesReturned": 1,
            "confidenceThreshold": 0.5,
        };

        const options_identify = {
            uri: uriBase + "/identify",
            body: JSON.stringify(params_identify),
            headers: {
                "Content-Type": "application/json",
                "Ocp-Apim-Subscription-Key" : subscriptionKey,
            },
        };

        request.post(options_identify, (error, response, body) => {
            if (error) {
                console.log("Error: ", error);
                return;
            }

            const jsonResponse = JSON.parse(body);
            if (jsonResponse[0].candidates[0] !== undefined) {
                resolve(jsonResponse[0].candidates[0].personId);
            } else {
                reject(new Error());
            }

        });
    });

}

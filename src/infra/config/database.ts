import { Sequelize } from "sequelize";
export const database = new Sequelize({
    database: "db",
    username: "root",
    password: "password",
    dialect: "mysql",
    host: "172.17.0.1",
    port: 3306,
});

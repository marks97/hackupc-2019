# HackUPC 2019

This is our project for HackUPC 2019

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them


* [Docker compose](https://docs.docker.com/compose/install/)


```
You also need to be able to execute make files
```

### Installing

A step by step series of how to get a development env running

```
1 Set up environment variables (.env file)
2. Run the following make commands on project root:

make setup
make install
make dev

3. Open http://localhost:3000 (or .env port).

```

## Running the tests

How to run the automated tests for this system


```
npm run test
```

## Built With

* [Typescript](https://www.npmjs.com/package/typescript)
* [Nodemon](https://www.npmjs.com/package/nodemon)
* [Websockets](https://www.npmjs.com/package/ws)
* [Chai](https://www.npmjs.com/package/chai)
* [Mocha](https://www.npmjs.com/package/mocha) 


## Authors

* **Marc Amorós**
